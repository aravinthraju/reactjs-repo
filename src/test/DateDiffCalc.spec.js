import React from 'react';
import {shallow} from 'enzyme';
import DateDifferenceCalc from './../app/index';
describe('<DateDifferenceCalc />', () => {
  const app  = shallow(<DateDifferenceCalc/>);
  beforeEach(() => {
    app.setState({startDate:'',
    endDate:'',
    startDateValid:true,
    endDateValid:true,
    difference:'',
    triggered:false})
})

  it('should render correctly', () => {
    expect(app).toMatchSnapshot();
  });

  it('should render initial state', () => {
    expect(app.state('startDate')).toBe('');
    expect(app.state('endDate')).toBe('');
    expect(app.state('startDateValid')).toBe(true);
    expect(app.state('endDateValid')).toBe(true);
    expect(app.state('difference')).toBe('');
    expect(app.state('triggered')).toBe(false);
  });

  it('test calculateDiff method with initial state', () => {
    app.instance().calculateDiff();
    expect(app.state('startDateValid')).toBe(false);
    expect(app.state('endDateValid')).toBe(false);
  });

  it('test reset method with initial state', () => {
    app.instance().resetDates();
    expect(app.state('startDate')).toBe('');
    expect(app.state('endDate')).toBe('');
    expect(app.state('startDateValid')).toBe(true);
    expect(app.state('endDateValid')).toBe(true);
    expect(app.state('difference')).toBe('');
    expect(app.state('triggered')).toBe(false);
  });

  it('vaild start date to return valid state', () => {
    app.setState({startDate:'10 10 2001'});
    app.instance().validateDate();
    expect(app.state('startDateValid')).toBe(true);
  });

  it('vaild start date to return valid state', () => {
    app.setState({startDate:'10 01 2001'});
    app.instance().validateDate();
    expect(app.state('startDateValid')).toBe(true);
  });

  it('vaild start date to return valid state', () => {
    app.setState({startDate:'29 02 2004'});
    app.instance().validateDate();
    expect(app.state('startDateValid')).toBe(true);
  });

  it('vaild start date to return valid state', () => {
    app.setState({startDate:'29 02 2000'});
    app.instance().validateDate();
    expect(app.state('startDateValid')).toBe(true);
  });
  it('vaild start date to return valid state', () => {
    app.setState({startDate:'29 11 2000'});
    app.instance().validateDate();
    expect(app.state('startDateValid')).toBe(true);
  });

  it('vaild start date to return valid state', () => {
    app.setState({startDate:'09 02 2003'});
    app.instance().validateDate();
    expect(app.state('startDateValid')).toBe(true);
  });

  it('invaild start date to return invalid state', () => {
    app.setState({startDate:'abc123243234'});
    app.instance().validateDate();
    expect(app.state('startDateValid')).toBe(false);
  });

  it('invaild start date to return invalid state', () => {
    app.setState({startDate:'30 02 2001'});
    app.instance().validateDate();
    expect(app.state('startDateValid')).toBe(false);
  });

  it('invaild start date to return invalid state', () => {
    app.setState({startDate:'31 11 2001'});
    app.instance().validateDate();
    expect(app.state('startDateValid')).toBe(false);
  });


  it('vaild end date to returns valid state', () => {
    app.setState({endDate:'10 10 2001'});
    app.instance().validateDate();
    expect(app.state('endDateValid')).toBe(true);
  });
  it('vaild start date to return valid state', () => {
    app.setState({endDate:'29 02 2004'});
    app.instance().validateDate();
    expect(app.state('endDateValid')).toBe(true);
  });

  it('vaild start date to return valid state', () => {
    app.setState({endDate:'29 02 2000'});
    app.instance().validateDate();
    expect(app.state('endDateValid')).toBe(true);
  });

  it('vaild start date to return valid state', () => {
    app.setState({endDate:'28 02 2001'});
    app.instance().validateDate();
    expect(app.state('endDateValid')).toBe(true);
  });

  it('vaild start date to return valid state', () => {
    app.setState({endDate:'29 11 2000'});
    app.instance().validateDate();
    expect(app.state('endDateValid')).toBe(true);
  });

  it('invaild end date to returns invalid state', () => {
    app.setState({endDate:'abc123243234'});
    app.instance().validateDate();
    expect(app.state('endDateValid')).toBe(false);
  });

  it('invaild end date to returns invalid state', () => {
    app.setState({endDate:'31 11 2001'});
    app.instance().validateDate();
    expect(app.state('endDateValid')).toBe(false);
  });

  it('invaild end date to returns invalid state', () => {
    app.setState({endDate:'31 02 2001'});
    app.instance().validateDate();
    expect(app.state('endDateValid')).toBe(false);
  });

  it('test calculateDiff method with valid dates', () => {
    app.setState({startDateValid:true});
    app.setState({endDateValid:true});
    app.setState({startDate:'10 02 2001'});
    app.setState({endDate:'10 02 2002'});
    app.instance().calculateDiff();
    expect(app.state('difference')).toBe(365);
    expect(app.state('triggered')).toBe(true);
  });

  it('test start date onchange handler', () => {
    var event ={target:{value:'12 12 2001'}}
    app.instance().startDateHandler(event);
    expect(app.state('startDate')).toBe('12 12 2001');
  });

  it('test end date onchange handler', () => {
    var event ={target:{value:'12 12 2001'}}
    app.instance().endDateHandler(event);
    expect(app.state('endDate')).toBe('12 12 2001');
  });

  it('test end date onchange handler', () => {
    app.find('input').first().simulate('change', {target: {value: '12 12 2001'} });
    expect(app.state('startDate')).toBe('12 12 2001');
  });
  it('test end date onchange handler', () => {
    app.find('input').at(1).simulate('change', {target: {value: '12 12 2003'} });
    expect(app.state('endDate')).toBe('12 12 2003');
  });

});