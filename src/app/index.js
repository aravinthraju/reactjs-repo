import React from "react";
import ReactDOM  from "react-dom";
import {Button, ButtonToolbar} from  "react-bootstrap";

export default class DateDifferenceCalc extends React.Component{ 
    constructor(props){
        super();
        this.state={
            startDate:'',
            endDate:'',
            startDateValid:true,
            endDateValid:true,
            difference:'',
            triggered:false
        }
    }
    /**event Handler for start date input */
    startDateHandler(event){
        this.setState({startDate:event.target.value,triggered:false})
       }

    /**event Handler for end date input */
    endDateHandler(event){
        this.setState({endDate:event.target.value,triggered:false});
    }

    /** get dates from state and calculates the difference*/
    calculateDiff(){
        if(this.state.startDate != '' && this.state.startDateValid && this.state.endDate !='' && this.state.endDateValid){
            var startDateArr = this.state.startDate.split(" ");
            var endDateArr = this.state.endDate.split(" ");
            var diffIndays = Math.round(((new Date(endDateArr[2], endDateArr[1]-1, endDateArr[0]) - new Date(startDateArr[2], startDateArr[1]-1, startDateArr[0]))/(1000*60*60*24)));
            this.setState({difference:diffIndays,triggered:true});
        }
        else{
            if(this.state.startDate ==''){
                this.setState({startDateValid:false});
            }
            if(this.state.endDate ==''){
                this.setState({endDateValid:false})
            }
        }
        
    }

    /** validate the startDate and endDate from state using RegEx */
    validateDate(){
        var regeExp = /^((0)[1-9]|(1)[0-9]|(2)[0-9]|(3)[0-1])( )(((0)[1-9])|((1)[0-2]))( )(19|20)\d{2}$/;
        var startDateArr = this.state.startDate.split(" ");
        var endDateArr = this.state.endDate.split(" ");
        if(this.state.startDate !='' && !(regeExp.test(this.state.startDate))){
           this.setState({startDateValid:false});
        }
        else{
            if(startDateArr[1] == '02'){
                if((startDateArr[2] % 4 == 0  && ((startDateArr[2] % 100 != 0) || startDateArr[2] % 400 == 0)) &&  startDateArr[0] <= '29' ){
                    this.setState({startDateValid:true});
                }
                else if (startDateArr[0] <= '28'){
                    this.setState({startDateValid:true});
                }
                else{
                    this.setState({startDateValid:false});
                }
            }
            if((startDateArr[1] == '01' || startDateArr[1] == '03' || startDateArr[1] == '05' || startDateArr[1] == '07' || startDateArr[1] == '08' || startDateArr[1] == '10' || startDateArr[1] == '12') ){
                if( startDateArr[0] <= 31){
                    this.setState({startDateValid:true});
                }
            }
            if((startDateArr[1] == '04' || startDateArr[1] == '06' || startDateArr[1] == '09' || startDateArr[1] == '11') ){
                if(startDateArr[0] <= 30 ){
                    this.setState({startDateValid:true});
                }
                else{
                    this.setState({startDateValid:false});
                }
            }
        }
        if(this.state.endDate !='' && !(regeExp.test(this.state.endDate))){
            this.setState({endDateValid:false})
         }
         else{
            if(endDateArr[1] == '02'){
                if((endDateArr[2] % 4 == 0  && ((endDateArr[2] % 100 != 0) || endDateArr[2] % 400 == 0)) &&  endDateArr[0] <= '29' ){
                    this.setState({endDateValid:true});
                }
                else if (endDateArr[0] <= '28'){
                    this.setState({endDateValid:true});
                }
                else{
                    this.setState({endDateValid:false});
                }
            }
            if((endDateArr[1] == '01' || endDateArr[1] == '03' || endDateArr[1] == '05' || endDateArr[1] == '07' || endDateArr[1] == '08' || endDateArr[1] == '10' || endDateArr[1] == '12') ){
                if( endDateArr[0] <= 31){
                    this.setState({endDateValid:true});
                }
            }
            if((endDateArr[1] == '04' || endDateArr[1] == '06' || endDateArr[1] == '09' || endDateArr[1] == '11') ){
                if(endDateArr[0] <= 30 ){
                    this.setState({endDateValid:true});
                }
                else{
                    this.setState({endDateValid:false});
                }
            }
         }
    }
    /** reset the state to initial on click of cancel */
    resetDates(){
        this.setState({
            startDate:'',
            endDate:'',
            difference:'',
            triggered:false,
            startDateValid:true,
            endDateValid:true,
        });
    }
    /**
     * Component Render method
     */
    render(){
        var result ="";
        var startDateMessage ="";
        var endDateMessage ="";
        if(this.state.triggered){
            result =  <div className="col-md-12 text-success" style={{fontSize:"20px",textAlign:"center"}}>{this.state.startDate}, {this.state.endDate}, {this.state.difference}</div>;
        }     
        if(!this.state.startDateValid){
            startDateMessage = <span className="text-warning" style={{paddingLeft:"20px"}}>Enter vaild date in DD MM YYYY</span>;
        }
        if(!this.state.endDateValid){
            endDateMessage = <span className="text-warning" style={{paddingLeft:"20px"}}>Enter vaild date in DD MM YYYY</span>;
        }
        return(<div>
           <div className="page-header" style={{textAlign:"center"}}>
                <h3>To calculate date difference in days(1900 - 2099)</h3>
           </div>
           <div className="container">
                <label className="col-md-2">Start Date </label>
                    <input type="text" value={this.state.startDate} onChange={(event) => this.startDateHandler(event)} placeholder="DD MM YYYY" maxLength="10" onBlur={this.validateDate.bind(this)}></input>
                    {startDateMessage}
                    <br/>
                    <br/>
                    <label className="col-md-2">End Date  </label>
                    <input type="text" value={this.state.endDate} onChange={(event) => this.endDateHandler(event)} onBlur={this.validateDate.bind(this)} placeholder="DD MM YYYY" maxLength="10"></input>
                    {endDateMessage}
                    <br/>
                    <br/>
                    <ButtonToolbar>
                        <Button bsStyle="primary" onClick={this.calculateDiff.bind(this)} disabled={!this.state.startDateValid || !this.state.endDateValid }>Calculate difference</Button>
                        <Button bsStyle="primary" onClick={this.resetDates.bind(this)}>Reset</Button>
                    </ButtonToolbar>
           </div>
           <hr/>
           {result}
        </div>);
    }
}
ReactDOM.render(<DateDifferenceCalc />, document.getElementById('dateDifferentCalc')|| document.createElement('div'),);