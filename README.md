# Calculate the difference between two dates - REACTJS
- App can read pairs of dates with the following format DD MM YYYY
- Displays difference between the two dates in days.
- input range of dates from 1900 to 2099

# Installation
Use NPM package manager to install
```bash
npm install
```
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

### `npm test`

Runs the test

### `npm run lint`

Run the rules and shows the errors

# REST API
Below is  assumption if our app function is to be made as REST API

## URL
Sample URL pattern with startDate and endDate as query params 

../calculator/data?startDate=11%2011%202001&endDate=11%2011%202002

## REQUEST JSON

Sample request

{
    startDate:'11 11 2001',
    endDate: '11 11 2002'
}

## RESPONSE JSON

Sample response

{
    result:365
}